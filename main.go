package main
//test dest
import (
	"context"
	"database/sql"
	"os"
    "fmt"
)

func main() {
    fmt.Println("main")
}

func foo() {
    // foo
}

func bar() {
    // bar
}

func secret() {
    username := "admin"
    var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	//var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"


    fmt.Println("Doing something with: ", username, password)
}

func secret() {
    username := "admin"
    var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	
	var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"

    fmt.Println("Doing something with: ", username, password)
}

func secret2() {
    username := "admin"
	
    var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"


    fmt.Println("Doing something with: ", username, password)
}


// func foo1() {
// 	db, err := sql.Open("sqlite3", ":memory:")
// 	if err != nil {
// 		panic(err)
// 	}
// 	rows, err := db.Query("SELECT * FROM foo WHERE name = " + os.Args[1])
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer rows.Close()
// }

func foo2() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query("select * from foo where name = " + os.Args[1])
	if err != nil {
		panic(err)
	}
	defer rows.Close()	
}

func foo3() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.QueryContext(context.Background(), "select * from foo where name = "+os.Args[1])
	if err != nil {
		panic(err)
	}
	defer rows.Close()
}

func foo4() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	tx, err := db.Begin()
	if err != nil {
		panic(err)
	}
	defer tx.Rollback()
	rows, err := tx.QueryContext(context.Background(), "select * from foo where name = "+os.Args[1])
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	if err := tx.Commit(); err != nil {
		panic(err)
	}
}

func foo5() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query("SELECT * FROM foo" + "WHERE name = " + os.Args[1])
	if err != nil {
		panic(err)
	}
	defer rows.Close()
}

func foo6() {
	var staticQuery1 = "SELECT * FROM foo WHERE age < "
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query(staticQuery1 + "32")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
}

const age = "32"
const gender = "M"

func foo7() {
	var staticQuery2 = "SELECT * FROM foo WHERE age < "
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query(staticQuery2 + age)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
}

func foo8() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query("SELECT * FROM foo WHERE gender = " + gender)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
}
