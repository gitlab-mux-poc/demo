#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/eval.py
# hash:  8eee173

import os

print(eval("os.getcwd()"))
print(eval("os.chmod('%s', 0777)" % 'test.txt'))
print(eval("os.getcwd()"))


print(eval("os.getcwd()"))
print(eval("os.chmod('%s', 0777)" % 'test.txt'))
print(eval("os.getcwd()"))

# A user-defined method named "eval" should not get flagged.
class Test(object):
    def eval(self):
        print("hi")
    def foo(self):
        self.eval()



Test().eval()
